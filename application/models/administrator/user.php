<?php

class User extends MY_Model {

    public $_table = 'user';
    public $primary_key = 'username';
    public $validate = array(
        array(
            'field' => 'user[current_password]',
            'label' => 'Current Password',
            'rules' => 'required|callback_validate_old_password'
        ),
        array(
            'field' => 'user[password]',
            'label' => 'Password',
            'rules' => 'required|matches[confirm_password]|min_length[5]'
        ),
        array(
            'field' => 'user[confirm_password]',
            'label' => 'Password Confirmation',
            'rules' => 'required'
        )
    );
    public $forget_password_validation = array(
        array(
            'field' => 'username',
            'label' => 'Username',
            'rules' => 'required|trim|callback_validate_username_exists'
        ),
    );

    function checkuser($username, $password) {
        $user = $this->get_by(array(
            'username' => $username,
            'password' => $password
        ));
        if (count($user))
            return $user->username;
        else
            return 0;
    }

    public function isLoggedIn() {
        return (bool) $this->session->userdata('logged_in');
    }

}
