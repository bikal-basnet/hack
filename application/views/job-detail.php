<div class="content-area">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <h2><?php echo $job->title ?></h2>
                <?php echo $job->job_description ?>
            </div>
            <div class="col-md-4 job-sidebar">
                <div><span class="title">Job Type: </span>
                    <span class="value"><?php echo $job->job_type ?></span></div>
                <div><span class="title">Company: </span>
                    <span class="value"><?php echo $job->company ?></span></div>
                <div><span class="title">Salary: </span>
                    <span class="value"><?php echo $job->salary ?></span></div>
                <div><span class="title">Category: </span>
                    <span class="value"><?php echo $job->category ?></span></div>
                <div><span class="title">City: </span>
                    <span class="value"><?php echo $job->city ?></span></div>
                <div>
                    <button class="navbar-btn nav-button wow bounceInRight login"  data-toggle="modal" data-target="#applyModal" data-wow-delay="0.8s">Apply Now</button>
                </div>

            </div>
        </div>
    </div>
    <hr>

</div>
<div class="modal fade" id="applyModal" tabindex="-1" role="dialog" aria-labelledby="applyModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="applyModalLabel">Apply For Job</h4>
            </div>
            <form id="apply-form" method="post" action="<?php echo base_url("jobs/apply"); ?>">
                <div class="modal-body">
                    <p style="display:none"  id="apply-form-message" class="alert alert-danger"></p>
                    <div class="form-group">
                        <label for="username" class="control-label">Cover Letter:</label>
                        <textarea name="cover_letter" id="" style="width:100%" rows="10"></textarea>
                    </div>
                    <input type="hidden" name="job_id" value="<?php echo $job->id ?>">
                    <div class="form-group">
                        <label for="cv" class="control-label">Attached CV</label>
                        <?php if(file_exists($user->cv)) {
                            echo '<br /><a href="' . $user->cv .'" target="_blank" />. '. $user->full_name .'-CV.pdf</a>';
                        }else {
                            echo 'Please add CV in your profile';
                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button <?php if(!file_exists($user->cv)) echo 'disabled' ?> type="submit" class="btn btn-primary">Apply</button>
                </div>
            </form>
        </div>
    </div>
</div>