<div class="content-area">
    <div class="container">
        <section style="padding-bottom: 50px; padding-top: 50px;">
            <div class="row">
                <form action="<?php echo base_url("login/updatePrimaryDetail") ?>" method="POST">
                <div class="col-md-4">
                    <img class="img-rounded img-responsive" src="<?php echo $user->profile_image; ?>">
                    <br>
                    <label for="full-name">Registered Name</label>
                    <input name="fullName" id="full-name" type="text" value="<?php echo $user->full_name; ?>" class="form-control">
                    <label for="email">Registered Email</label>
                    <input name="email" id="email" type="text" value="<?php echo $user->email; ?>" class="form-control">
                    <br>
                    <button type="submit" class="btn btn-success" href="#">Update Details</button>
                    <br><br>
                    <h3>Change Your Password</h3>
                    <br>
                    <label>Enter Old Password</label>
                    <input type="password" class="form-control">
                    <label>Enter New Password</label>
                    <input type="password" class="form-control">
                    <label>Confirm New Password</label>
                    <input type="password" class="form-control">
                    <br>
                    <a class="btn btn-warning" href="#">Change Password</a>
                </div>
                </form>
                <div class="col-md-8">
                    <form action="<?php echo base_url("login/updateDetail") ?>" method="post">
                    <div class="alert alert-info">
                        <h2>About me</h2>
                        <textarea style="width:100%" name="bio" id="bio" rows="5"><?php echo $user->bio ?></textarea>
                    </div>
                    <div>
                        <label for="facebook">Facebook</label>
                        <input name="facebook" id="facebook" type="text" value="<?php echo $user->facebook; ?>" class="form-control">
                        <label for="twitter">Twitter</label>
                        <input name="twitter" id="twitter" type="text" value="<?php echo $user->twitter; ?>" class="form-control">
                        <label for="googleplus">Google Plus</label>
                        <input name="googleplus" id="googleplus" type="text" value="<?php echo $user->googleplus; ?>" class="form-control">
                        <label for="linkedin">LinkedIn</label>
                        <input name="linkedin" id="linkedin" type="text" value="<?php echo $user->linkedin; ?>" class="form-control">
                        <label for="">CV</label>
                        <?php
                        if(file_exists($user->cv)) {
                            echo '<br /><a href="' . $user->cv .'" target="_blank" /> My CV</a>';
                        }
                        ?>
                        <input name="cv" id=cv" type="file" >
                        <br>
                        <br>
                        <button type="submit" class="btn btn-success" href="#">Update Details</button>
                        <br><br>
                    </div>
                    </form>
                </div>
            </div>
            <!-- ROW END -->


        </section>
    </div>
</div>