<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin</title>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>

    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
    <link rel="icon" href="favicon.ico" type="image/x-icon">

    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/fontello.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/owl.theme.css">
    <link rel="stylesheet" href="css/owl.transitions.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="responsive.css">
    <script src="js/vendor/modernizr-2.6.2.min.js"></script>
    <style>
        .form-inline .form-group {
            display: block;
            margin-bottom: 20px;
            vertical-align: middle;
        }
    </style>
</head>
<body>

<div class="content-area">
    <div class="container">
        <div class="search-form wow pulse" >
            <form action="" class=" form-inline">
                <div class="form-group">
                    <label for="title">Job Title</label>
                    <input id="title" type="text" class="form-control" placeholder="Job Key Word">
                </div>
                <div class="form-group">
                    <label for="job_type">Job Type</label>
                    <select name="" id="job_type" class="form-control">
                        <option>Job Type</option>
                        <option value="Full Time" selected>Full Time</option>
                        <option value="Part Time">Part Time</option>
                        <option>New york, CA</option>
                        <option>New york, CA</option>
                    </select>
                </div>

                <div class="form-group">
                    <select name="" id="" class="form-control">
                        <option>Select Your Category</option>
                        <option selected>Graphic Design</option>
                        <option>Web Design</option>
                        <option>App Design</option>
                    </select>
                </div>
                <input type="submit" class="btn" value="Search">


            </form>
        </div>
    </div>
    <hr>

   </body>
</html>
