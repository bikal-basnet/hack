<div class="content-area">
    <div class="container">
        <div class="row page-title text-center wow bounce" data-wow-delay="1s">
            <h2><span><?php echo $totalJob ?></span> Available jobs for you</h2>
        </div>
        <div class="row jobs">
            <div class="col-md-9">
                <div class="job-posts table-responsive">
                    <table class="table">
                        <?php
                        foreach($jobs as $i => $job) {
                            if($i%2 == 1) $class = 'odd wow fadeInUp';
                            else $class = "odd wow fadeInUp";
                            ?>
                        <tr class="<?php echo $class ?>" data-wow-delay="1s">
                            <td class="tbl-logo"><img width="70" height="70" src="<?php echo base_url($job->image); ?>" alt=""></td>
                            <td class="tbl-title"><h4><?php echo $job->title ?> <br><span class="job-type"><?php echo $job->job_type ?></span></h4></td>
                            <td><p><?php echo $job->company?></p></td>
                            <td><p><i class="icon-location"></i><?php echo $job->city ?></p></td>
                            <td><p><?php echo $job->salary ?></p></td>
                            <td class="tbl-apply"><a href="<?php echo base_url("jobs/detail/$job->id"); ?>">View Detail</a></td>
                        </tr>
                        <?php } ?>
                    </table>
                </div>
                <div class="more-jobs">
                    <a href=""> <i class="fa fa-refresh"></i>View more jobs</a>
                </div>
            </div>
            <div class="col-md-3 hidden-sm">
                <div class="job-add wow fadeInRight" data-wow-delay="1.5s">
                    <h2>Seeking a job?</h2>
                    <a href="#">Create a Account</a>
                </div>
            </div>
        </div>
    </div>
</div>