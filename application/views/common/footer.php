<div class="footer-area">
    <div class="container">

        <div class="row text-center text text-capitalize">
            <p>Made with love and passion at hacklash 2016 :)</p>
        </div>
    </div>
</div>


<script>
    var BASE_URL = "<?php echo base_url(); ?>"
</script>
<script src="<?php echo base_url(); ?>js/vendor/jquery-1.10.2.min.js"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-1.10.2.min.js"><\/script>')</script>
<script src="<?php echo base_url(); ?>js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>js/owl.carousel.min.js"></script>
<script src="<?php echo base_url(); ?>js/wow.js"></script>
<script src="<?php echo base_url(); ?>js/main.js"></script>

<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Login</h4>
            </div>
            <form id="login-form" method="post" action="<?php echo base_url("login/check"); ?>">
            <div class="modal-body">
                <p style="display:none"  id="login-form-message" class="alert alert-danger"></p>
                    <div class="form-group">
                        <label for="username" class="control-label">Username:</label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="password" class="control-label">Password:</label>
                        <input type="password" class="form-control" id="password"  name="password">
                    </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Login</button>
            </div>
            </form>
        </div>
    </div>
</div>

    </body>
</html>