<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>HACK</title>
        <meta name="description" content="">
        <meta name="author" content="">
        <meta name="keyword" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

<!--        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,700,800' rel='stylesheet' type='text/css'>-->

        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">

        <link rel="stylesheet" href="<?php echo base_url() ?>css/normalize.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/fontello.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/animate.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/owl.theme.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>responsive.css">
        <script src="<?php echo base_url() ?>js/vendor/modernizr-2.6.2.min.js"></script>
    </head>
	<body>
        <div id="preloader">
            <div id="status">&nbsp;</div>
        </div>
        <!-- Body content -->


        <nav class="navbar navbar-default">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#"><img src="<?php echo base_url() ?>img/logo.png" alt=""></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
              <div class="button navbar-right">
                  <div id="login-signup-wrapper" class="active">
                      <button class="navbar-btn nav-button wow bounceInRight login"  data-toggle="modal" data-target="#loginModal" data-wow-delay="0.8s">Login</button>
                      <button class="navbar-btn nav-button wow fadeInRight" data-wow-delay="0.6s">Sign up</button>
                  </div>
                  <div id="acc-wrapper">
                      <a href="<?php echo base_url("me"); ?>">
                          <img width="40" src="" style="border-radius: 60px;">
                          <span></span>
                      </a>
                  </div>
              </div>
              <ul class="main-nav nav navbar-nav navbar-right">
                <li class="wow fadeInDown" data-wow-delay="0s"><a <?php if(uri_string() == '') echo 'active' ?> href="<?php echo base_url() ?>">Home</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.1s"><a <?php if(uri_string() == 'jobs') echo 'class="active"' ?> href="<?php echo base_url("jobs") ?>">Jobs</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.2s"><a  <?php if(uri_string() == 'idea') echo 'class="active"' ?> href="idea">I have an idea</a></li>
                <li class="wow fadeInDown" data-wow-delay="0.3s"><a <?php if(uri_string() == 'about-us') echo 'class="active"' ?> href="#">About us</a></li>
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>
