<?php
$active_sub_menu = "";
$active_menu = $this->uri->segment(2);
if ($active_menu == "submenucontent") {
    $_submenu = $this->submenu->get($this->uri->segment(4));
    if (empty($_submenu)) {
        $_submenucontent = $this->submenucontent->get($this->uri->segment(4));
        $_submenu = $this->submenu->get($_submenucontent->submenu_id);
    }
    $active_sub_menu = $_submenu->menu_internal_name;
} else if ($active_menu == "submenu") {
    if ($this->uri->segment(4)) {
        $submenu = $this->submenu->get($this->uri->segment(4));
        if (!empty($submenu)) {
            $active_sub_menu = $submenu->menu_internal_name;
        } else {
            $active_sub_menu = $this->uri->segment(4);
        }
    }
}
?>
<div id="sidebar-wrapper">
 <ul class="sidebar-nav">
  <li class="sidebar-brand"><a id="brand-logo" href="#"></a></li>
  <li>
   <a class="<?php if ($active_menu == "dashboard" OR $active_menu == '') echo "active" ?>" href="<?php echo base_url('administrator/dashboard') ?>"><i class="glyphicon glyphicon-home"></i> Dashboard</a>
  </li>
  <li>
   <a class="<?php if ($active_menu == "menu") echo "active" ?>" href="<?php echo base_url('administrator/menu') ?>"><i class="glyphicon glyphicon-list"></i> Menu</a>
  </li>
  <li>
   <a class="<?php if ($active_menu == "work" || $active_menu == "gallery" || $active_menu == "news") echo "active" ?>" href="<?php echo base_url('administrator/work') ?>"><i class="glyphicon glyphicon-credit-card"></i> Work</a>
  </li>
  <li>
   <a class="<?php if ($active_sub_menu == "press") echo "active" ?>" href="<?php echo base_url('administrator/submenu/view/press') ?>"><i class="glyphicon glyphicon-comment"></i> Press</a>
  </li>
  <li>
   <a class="<?php if ($active_sub_menu == "profile") echo "active" ?>" href="<?php echo base_url('administrator/submenu/view/profile') ?>"><i class="glyphicon glyphicon-user"></i> Profile</a>
  </li>
  <li>
   <a class="<?php if ($active_sub_menu == "events") echo "active" ?>" href="<?php echo base_url('administrator/submenu/view/events') ?>"><i class="glyphicon glyphicon-camera"></i> Events</a>
  </li>
  <li>
   <a class="<?php if ($active_sub_menu == "connect") echo "active" ?>" href="<?php echo base_url('administrator/submenu/view/connect') ?>"><i class="glyphicon glyphicon-earphone"></i> Connect</a>
     </li>
     <li>
      <a class="<?php if ($active_menu == "site") echo "active" ?>" href="<?php echo base_url('administrator/site') ?>"><i class="glyphicon glyphicon-adjust"></i> Site Settings</a>
     </li>
  <li>
   <a href="<?php echo base_url('administrator/login/logout') ?>"><i class="glyphicon glyphicon-log-out"></i> Logout</a>
  </li>
 </ul>
</div>
<!--<a href="#menu-toggle" class="btn btn-default" id="menu-toggle"><i class="glyphicon glyphicon-arrow-left"></i></a>-->