<!-- /#wrapper -->
<!-- Menu Toggle Script -->
<script>
    $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        var icon = $(this).find("i");
        if(icon.hasClass("glyphicon-arrow-left")) {
            icon.removeClass("glyphicon-arrow-left")
            icon.addClass("glyphicon-arrow-right");
        }else {
            icon.removeClass("glyphicon-arrow-right")
            icon.addClass("glyphicon-arrow-left");
        }
    });
</script>
</body>
</html>