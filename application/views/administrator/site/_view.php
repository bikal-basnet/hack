<div class="col-lg-12">
 <div class="panel panel-default">
  <div class="panel-heading">
   Menu Listings
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
   <div class="table-responsive">
    <div>
     <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
      <thead>
       <tr>
        <th>S.No.</th>
        <th>Internal Name</th>
        <th>Display Name</th>
        <th>Priority</th>
        <th>Created on</th>
        <th>Last updated on</th>
        <th></th>
       </tr>
      </thead>
      <tbody>
          <?php
          $i = 0;
          foreach ($menus as $menu) {
              ?>
           <tr>
            <td><?php echo ++$i; ?></td>
            <td><?php echo $menu->display_name ?></td>
            <td><?php echo $menu->internal_name ?></td>
            <td><?php echo $menu->priority ?></td>
            <td><?php echo date("M-d-Y h:i", $menu->created_on); ?></td>
            <td><?php echo date("M-d-Y h:i", $menu->updated_on) ?></td>
            <td>
             <a class="btn btn-warning btn-xs" href="<?php echo base_admin("menu/form/$menu->id") ?>"><i class="glyphicon glyphicon-edit"></i> Edit</a>
            </td>
           </tr>
       <?php } ?>
      </tbody>
     </table>
    </div>
   </div>
  </div>
  <!-- /.panel-body -->
 </div>
 <!-- /.panel -->
</div>