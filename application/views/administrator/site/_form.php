<form action="<?php echo $form_action ?>" method="POST">
 <div class="row well">
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('site[internal_name]') ?>">
    <label>Site Name</label>
    <input class="form-control" name="site[site_name]" value="<?php echo $site->site_name ?>" />
    <?php echo form_error('site[site_name]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[google_analytics_code]') ?>">
    <label>Google analytics code</label>
    <textarea name="site[google_analytics_code]" class="form-control" style="height:300px;"><?php echo $site->google_analytics_code ?></textarea>
    <?php echo form_error('site[google_analytics_code]') ?>
   </div>
<!--   <div class="form-group <?php echo form_has_error('site[google_map_style_code]') ?>">
    <label>Google map style code</label>
    <textarea name="site[google_map_style_code]" class="form-control" style="height: 230px;"><?php echo $site->google_map_style_code ?></textarea>
    <?php echo form_error('site[google_map_style_code]') ?>
   </div>-->
   <div class="form-group <?php echo form_has_error('site[map_latitude]') ?>">
    <label>Map (Latitude)</label>
    <input type="text" class="form-control form-inline" name="site[map_latitude]" value="<?php echo $site->map_latitude ?>" />
    <?php echo form_error('site[map_latitude]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[map_longitude]') ?>">
    <label>Map (Longitude)</label>
    <input type="text" class="form-control form-inline" name="site[map_longitude]" value="<?php echo $site->map_longitude ?>" />
    <?php echo form_error('site[map_longitude]') ?>
   </div>
  </div>
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('site[facebook_link]') ?>">
    <label>Facebook link</label>
    <input class="form-control" name="site[facebook_link]" value="<?php echo $site->facebook_link ?>" />
    <?php echo form_error('site[facebook_link]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[twitter_link]') ?>">
    <label>Twitter link</label>
    <input class="form-control" name="site[twitter_link]" value="<?php echo $site->twitter_link ?>" />
    <?php echo form_error('site[twitter_link]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[pinterest_link]') ?>">
    <label>Pinterest link</label>
    <input class="form-control" name="site[pinterest_link]" value="<?php echo $site->pinterest_link ?>" />
    <?php echo form_error('site[pinterest_link]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[instagram_link]') ?>">
    <label>Instagram link</label>
    <input class="form-control" name="site[instagram_link]" value="<?php echo $site->instagram_link ?>" />
    <?php echo form_error('site[instagram_link]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[youtube_link]') ?>">
    <label>Youtube link</label>
    <input class="form-control" name="site[youtube_link]" value="<?php echo $site->youtube_link ?>" />
    <?php echo form_error('site[youtube_link]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[google_link]') ?>">
    <label>Google site link</label>
    <input class="form-control" name="site[google_link]" value="<?php echo $site->google_link ?>" />
    <?php echo form_error('site[google_link]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[improper_fonts]') ?>">
    <label>Improper font characters</label>
    <small>(Comma seperated value)</small>
    <textarea class="form-control" name="site[improper_fonts]"><?php echo $site->improper_fonts ?></textarea>
    <?php echo form_error('site[improper_fonts]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[facebook_page_id]') ?>">
    <label>Facebook Page ID</label> <small><a href="http://findmyfacebookid.com" target="_new"><i class="glyphicon glyphicon-link"></i> findmyfacebookid.com</a></small>
    <input class="form-control" name="site[facebook_page_id]" value="<?php echo $site->facebook_page_id ?>" />
    <?php echo form_error('site[facebook_page_id]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('site[instagram_user_id]') ?>">
    <label>Instagram Page ID</label> <small><a href="http://jelled.com/instagram/lookup-user-id" target="_new"><i class="glyphicon glyphicon-link"></i> jelled.com/instagram/lookup-user-id</a></small>
    <input class="form-control" name="site[instagram_user_id]" value="<?php echo $site->instagram_user_id ?>" />
    <?php echo form_error('site[instagram_user_id]') ?>
   </div>
  </div>
 </div>
 <div class="col-lg-12">
  <button class="btn btn-primary" type="submit">Submit</button>
 </div>
</form>
</div>