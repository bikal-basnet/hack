<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery-2.1.1.min.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery.ui.widget.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.iframe-transport.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.fileupload.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/custom-upload.js"></script>
<div class="image-upload-box col-lg-3 hide clone-image-holder">
 <div class="image-holder">
<!--  <a class=" edit-image btn btn-primary btn-xs"  data-toggle="modal" ><i class="glyphicon glyphicon-edit"></i> Edit</a>-->
  <a class="remove-image-holder btn btn-danger btn-xs" data-toggle="modal" data-target="#confirm-remove-image-holder"><i class="glyphicon glyphicon-trash"></i> Remove</a>
  <div class="form-group">
   <div class="upload-image-holder">
    <img width="228" src="" class="image-upload"/>
   </div>
  </div>
  <input class="icon-image" type="hidden" name="gallery[image][]" value="" />
  <input class="icon-image-thumb" type="hidden" name="gallery[image_thumb][]" value="" />
  <input type="hidden" name="gallery[id][]" value="0"/>
  <div class="form-group">
   <label>Title: </label> <input type="text" name="gallery[title][]" class="form-control image-tile" />
  </div>
  <div class="form-group">
   <label>Priority: </label> <input type="text" name="gallery[priority][]"  class="form-control image-priority"/>
  </div>
 </div>
</div>
<form action="<?php echo $form_action; ?>" method="POST" name="gallery_update">
 <div id="image-container" class="row well">
     <?php foreach ($images as $i => $gal_image) { ?>
      <div class="image-upload-box col-lg-3">
       <div class="image-holder">
        <!--<a class=" edit-image btn btn-primary btn-xs"  data-toggle="modal" data-target="#edit-modal-<?php echo $gal_image->id ?>" ><i class="glyphicon glyphicon-edit"></i> Edit</a>-->
        <a class="confirm-delete-btn btn btn-danger btn-xs" href="<?php echo base_admin("gallery/delete/$gal_image->id") ?>"><i class="glyphicon glyphicon-trash"></i> Delete</a>
        <div class="form-group">
         <div class="upload-image-holder">
          <img width="228" src="<?php echo base_url() . $gal_image->image_thumb ?>" />
         </div>
        </div>
        <input class="icon-image" type="hidden" name="gallery[image][]" value="<?php echo $gal_image->image ?>" />
        <input class="icon-image-thumb" type="hidden" name="gallery[image_thumb][]" value="<?php echo $gal_image->image_thumb ?>" />
        <input type="hidden" name="gallery[id][]" value="<?php echo $gal_image->id ?>"/>
        <div class="form-group">
         <label>Title: </label> <input type="text" name="gallery[title][]" class="form-control image-tile" value="<?php echo $gal_image->title ?>"/>
        </div>
        <div class="form-group">
         <label>Priority: </label> <input type="text" name="gallery[priority][]"  class="form-control image-priority" value="<?php echo $gal_image->priority ?>"/>
        </div>
       </div>
      </div>
  <?php } ?>
  <div id="gallery-upload-holder" class="col-lg-12">
   <input type="file" class="gallery-file-upload" name="upload_image" data-url="<?php echo base_admin("gallery/upload_file") ?>" multiple="">
   <!--<button class="add-more-image-btn btn btn-warning"><i class="glyphicon glyphicon-plus"></i> Add image</button>-->
   <button type="submit" class="btn btn-warning"><i class="glyphicon glyphicon-plus"></i> Save changes</button>
  </div>
 </div>
</form>
<script>
    (function () {
        $("form[name='gallery_update']").on("submit", function (e) {
            var form = $(this);
            $(this).find("small.error").remove();
            var priorities = form.find(".image-priority");
            var titles = form.find(".image-tile");
            var formSubmit = true;
            $.each(priorities, function (i, priority) {
                if($.trim($(this).val()) == "" && !$.isNumeric($(this).val())) {
                    $(this).addClass("error");
                    $(this).after("<small class='error'>Invalid priority.</small>");
                    formSubmit = false;
                }
            });
            $.each(titles, function (i, title) {
                if($.trim($(this).val()) == "") {
                    $(this).addClass("error");
                    $(this).after("<small class='error'>Invalid title.</small>");
                    formSubmit = false;
                }
            });
            if(!formSubmit) {
                return false;
            }else {
                return true;
            }
        });
    }());
    $(function () {
        $(document).on("click", ".gallery-file-upload", function () {
            $(".gallery-file-upload").fileupload({
                dataType: 'json',
                start: function () {
                    $(this).parents("form").attr("onsubmit", "return false;")
                    $(this).after("<img src='/mrad/administrator_resources/images/loading.gif' class='loading' />")
//            $("#submit-recipe-btn").removeClass("active"); // To make sure the form cant be submitted while upload is going on
                },
                stop: function () {
                    $(this).parents("form").removeAttr("onsubmit");
                    $(this).siblings(".loading").remove();
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                },
                done: function (e, data) {
                    if (!data.result.status) {
                        var errorDivEl = $('<p class="alert alert-danger">'+ data.result.error +'</p>');
                        $(".page-header").after(errorDivEl);
                        setTimeout(function() {
                            errorDivEl.remove();
                        }, 4000);
                        $(this).siblings('img.loading').remove();
                        return;
                    }
                    // new image element
                    var imageUploadHolderClone = $(".clone-image-holder").clone();
                    $('#image-container').find('#gallery-upload-holder').before(imageUploadHolderClone);
                    var imageUploadHolder = $('#image-container').find('#gallery-upload-holder').prev();
                    imageUploadHolder.removeClass("hide").removeClass("clone-image-holder");
                    imageUploadHolder.find(".image-upload").attr("src", data.result.full_file_path);
                    $(this).parents("form").removeAttr("onsubmit");
                    // hidden input tags
                    var targetInputElm = imageUploadHolder.find(".icon-image");
                    var targetInputThumbElm = imageUploadHolder.find(".icon-image-thumb");
                    targetInputElm.val(data.result.file_path);
                    targetInputThumbElm.val(data.result.thumb_path);

                },
                fail: function (e, data) {
                }
            });
        })
    });
</script>