<form action="<?php echo $form_action ?>" method="POST">
 <?php if(validation_errors()) { ?>
 <p class="alert alert-danger validation-error-holder"><?php echo validation_errors(); ?></p>
 <?php } ?>
 <div class="row well">
  <div class="col-lg-6">
   <div class="form-group">
    <label>Current Password</label>
    <input type="password" class="form-control" name="user[current_password]" value="" autofocus="" />
   </div>
   <div class="form-group">
    <label>New Password</label>
    <input type="password" class="form-control" name="user[password]" value="" />
   </div>
   <div class="form-group">
    <label>Confirm Password</label>
    <input type="password" class="form-control" name="user[confirm_password]" value="" />
   </div>
  </div>
  <div class="col-lg-12">
   <button class="btn btn-primary" type="submit">Submit</button>
  </div>
 </div>
</form>