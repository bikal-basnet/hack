<div class="row well" >
 <form action="<?php echo $form_action ?>" method="POST">
  <div class="col-lg-4">
   <div class="form-group <?php echo form_has_error('news[title]') ?>">
    <label>Title</label>
    <input class="form-control" name="news[title]" autofocus="" value="<?php echo $news->title ?>" />
    <?php echo form_error('news[title]') ?>
   </div>
  </div>
  <div class="col-lg-8">
   <div class="form-group <?php echo form_has_error('news[description]') ?>">
    <label>Description</label>
    <textarea class="form-control" name="news[description]"  style="height: 300px;"><?php echo $news->description ?></textarea>
    <?php echo form_error('news[description]') ?>
   </div>
  </div>
  <div class="col-lg-12">
   <button class="btn btn-primary" type="submit">Submit</button>
   <a class="btn btn-default" href="<?php echo base_admin("work"); ?>">Cancel</a>
  </div>
 </form>
</div>
<input type="hidden" id="base-url" value="<?php echo base_admin() ?>" />