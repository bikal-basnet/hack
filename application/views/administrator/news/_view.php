<div class="col-lg-12">
 <div class="panel panel-default">
  <div class="panel-heading">
   Work News Listings
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
   <div class="table-responsive">
    <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline" role="grid">

     <div id="action-wrapper" class="row">
      <div class="col-lg-10">
       <!--<label><input type="search" class="form-control input-sm" placeholder="Search"></label>-->
      </div>
      <div class="col-lg-2">
       <a href="<?php echo base_admin("news/form/$work->id") ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Add new News</a>
      </div>
     </div>

     <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
      <thead>
       <tr>
        <th>S.No.</th>
        <th>Image</th>
        <th>Title</th>
        <th>Status</th>
        <th>Created on</th>
        <th>Updated on</th>
        <th></th>
       </tr>
      </thead>
      <tbody>
          <?php
          $i = 0;
          foreach ($news_many as $news) {
              ?>
           <tr>
            <td><?php echo ++$i; ?></td>
            <td><img src="<?php echo base_url().$news->image_thumb?>" width="100"></td>
            <td><?php echo $news->title ?></td>
            <td><?php 
                if($news->status == 'Publish') 
                    echo '<i class="glyphicon glyphicon-eye-open green-text" title="Published"></i>';
                else
                    echo '<i class="glyphicon glyphicon-eye-close red-text" title="Unpublished"></i>';
                ?>
            </td>
            <td><?php echo date("M-d-Y h:i", $news->created_on); ?></td>
            <td><?php echo date("M-d-Y h:i", $news->updated_on) ?></td>
            <td>
             <a class="btn btn-warning btn-xs" href="<?php echo base_admin("news/edit/$news->id") ?>"><i class="glyphicon glyphicon-edit"></i> Edit</a>
             <a class="confirm-delete-btn btn btn-danger btn-xs" href="<?php echo base_admin("news/delete/$news->id") ?>"><i class="glyphicon glyphicon-trash"></i> Delete</a>
            </td>
           </tr>
       <?php } ?>
      </tbody>
     </table>

     <div class="row">
      <!--      <div class="col-lg-3">
             <label>
              <select class="form-control input-sm" aria-controls="dataTables-example" name="dataTables-example_length">
               <option value="10">10</option><option value="25">25</option>
               <option value="50">50</option><option value="100">100</option>
              </select> records per page
             </label>
            </div>-->
      <div class="col-lg-9 text-right">
       <?php echo $this->pagination->create_links(); ?>
      </div>
     </div>
    </div>
   </div>
  </div>
  <!-- /.panel-body -->
 </div>
 <!-- /.panel -->
</div>