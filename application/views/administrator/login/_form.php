<div class="login-holder">
    <?php
    if ($this->session->flashdata('action_result')) {
        ?>
     <div class="alert alert-<?php echo $this->session->flashdata('action_result') ?>">
         <?php echo $this->session->flashdata('action_message') ?>
     </div>
     <?php
 }
 ?>
 <a id="logo"></a>
 <?php
 if (isset($error_message)) {
     echo '<p class="alert alert-danger">' . $error_message . '</p>';
 }
 ?>
 <form id="login_form" method="POST" action="">
  <div class="form-group">
   <input type="text" placeholder="Username" name="username" value="<?php echo set_value('username') ?>" autofocus="">
   <?php echo form_error('username') ?>
  </div>
  <div class="form-group">
   <input type="password" placeholder="Password" name="password">
   <?php echo form_error('password') ?>
  </div>
  <div class="text-right">
   <!--<input type="checkbox" id="remember-me" name="remember_me"> <label for="remember-me">Remember Me</label>-->
   <a href="#" data-toggle="modal" data-target="#forgot-password">Forgot Password ?</a>
  </div>
  <input type="submit" id="submit_login" class="btn btn-danger" name="submit" value="Submit">
 </form>
<!-- <small id="copyright-text"><?php echo date('Y') ?> © AMNIL Technologies Pvt. Ltd.</small>-->
</div>
<div class="modal fade" id="forgot-password" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
  <form action="<?php echo base_admin("login/forget_password") ?>" method="post">
   <div class="modal-content">
    <div class="modal-header"><h4>Forgot your password?</h4></div>
    <div class="modal-body">
     <div class="form-group">
      <label>Type your username</label>
      <input type="text" class="form-control" name="username"/>
     </div>
    </div>
    <div class="modal-footer">
     <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
     <button type="submit" href="#" class="btn btn-warning">Send</button>
    </div>
   </div>
  </form>
 </div>
</div>