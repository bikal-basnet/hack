<div class="row">
 <div class="col-lg-6">
  <form class="well" action="<?php echo $form_action ?>" method="POST">
   <div class="form-group <?php echo form_has_error('menu[internal_name]') ?>">
    <label>Internal Name</label>
    <input class="form-control" name="menu[internal_name]" value="<?php echo $menu->internal_name ?>" readonly="" />
    <?php echo form_error('menu[internal_name]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('menu[internal_name]') ?>">
    <label>Display Name</label>
    <input class="form-control" name="menu[display_name]" value="<?php echo $menu->display_name ?>" autofocus="" />
    <?php echo form_error('menu[display_name]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('menu[priority]') ?>">
    <label>Priority</label>
    <input class="form-control" name="menu[priority]" value="<?php echo $menu->priority ?>">
    <?php echo form_error('menu[priority]') ?>
   </div>
   <button class="btn btn-primary" type="submit">Submit</button>
   <a class="btn btn-default" href="<?php echo base_admin("menu"); ?>">Cancel</a>
  </form>
 </div>
</div>