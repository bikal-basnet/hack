<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery-2.1.1.min.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery.ui.widget.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.iframe-transport.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.fileupload.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/custom-upload.js"></script>
<div class="row well">
 <form action="<?php echo $form_action ?>" method="POST">
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('submenucontent[title]') ?>">
    <label>Title</label>
    <textarea class="form-control" name="submenucontent[title]" autofocus="" ><?php echo $content->title ?></textarea>
    <?php echo form_error('submenucontent[title]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('submenucontent[subtitle]') ?>">
    <label>Subtitle</label>
    <textarea class="form-control" name="submenucontent[subtitle]" ><?php echo $content->subtitle ?></textarea>
    <?php echo form_error('submenucontent[subtitle]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('submenucontent[heading]') ?>">
    <label>Heading</label>
    <textarea class="form-control" name="submenucontent[heading]" ><?php echo $content->heading ?></textarea>
    <?php echo form_error('submenucontent[heading]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('submenucontent[sub_heading]') ?>">
    <label>Sub heading</label>
    <textarea class="form-control" name="submenucontent[sub_heading]" ><?php echo $content->sub_heading ?></textarea>
    <?php echo form_error('submenucontent[sub_heading]') ?>
   </div>
   <div class="form-group">
    <label>Priority: </label> <input type="text" name="submenucontent[priority]" value="<?php echo $content->priority ?>" class="form-control"/>
   </div>
   <div class="form-group">
    <label>Status</label>
    <?php
    $options = array(
        'Publish' => 'Publish',
        'Unpublish' => 'Unpublish',
    );
    echo form_dropdown('submenucontent[status]', $options, $content->status, 'class="form-control"')
    ?>
   </div>
   <div class="form-group">
    <label>Link in News Feed? </label> <input type="checkbox" name="submenucontent[link_in_news_feed]" value="1" <?php if($content->link_in_news_feed) echo 'checked'; ?> />
   </div>
  </div>
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('submenucontent[image]') ?>">
    <input id="icon-image" type="hidden" name="submenucontent[image]" value="<?php echo $content->image ?>" />
    <input id="icon-image-thumb" type="hidden" name="submenucontent[image_thumb]" value="<?php echo $content->image ?>" />
    <label>Image</label>
    <?php if ($submenu->submenu_internal_name == "location") { ?>
        <small>484 X 181</small>
        <input type="file" class="file-input-ajax" name="upload_image" data-url="<?php echo base_admin("submenucontent/upload_file/location"); ?>" data-target="#icon-image" data-target-thumb="#icon-image-thumb"/>
    <?php } else { ?>
        <small>189 X 189</small>
        <input type="file" class="file-input-ajax" name="upload_image" data-url="<?php echo base_admin("submenucontent/upload_file"); ?>" data-target="#icon-image" data-target-thumb="#icon-image-thumb"/>
    <?php } ?>
    <?php if ($content->image) { ?>
        <div class="upload-image-holder">
         <img src="<?php echo base_url() . $content->image ?>" />
         <span class="remove-image"><i class="glyphicon glyphicon-remove"></i></span>
        </div>
    <?php } ?>
    <?php echo form_error('submenucontent[image]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('submenucontent[description]') ?>">
    <label>Description</label>
    <textarea class="form-control" name="submenucontent[description]" style="height: 350px"><?php echo $content->description ?></textarea>
    <?php echo form_error('submenucontent[description]') ?>
   </div>
  </div>
  <div class="col-lg-12">
   <button class="btn btn-primary" type="submit">Submit</button>
   <a class="btn btn-default" href="<?php echo base_admin("submenucontent/view/$submenu->id"); ?>">Cancel</a>
  </div>
 </form>
</div>
<input type="hidden" id="base-url" value="<?php echo base_admin() ?>" />