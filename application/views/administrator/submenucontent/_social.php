<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery-2.1.1.min.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery.ui.widget.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.iframe-transport.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.fileupload.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/custom-upload.js"></script>
<div class="row well">
 <form action="<?php echo $form_action ?>" method="POST">
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('submenucontent[title]') ?>">
    <label>Title</label>
    <input class="form-control" name="submenucontent[title]" autofocus="" value="<?php echo $content->title ?>" />
    <?php echo form_error('submenucontent[title]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('submenucontent[link]') ?>">
    <label>Link</label>
    <input class="form-control" name="submenucontent[link]" autofocus="" value="<?php echo $content->link ?>" />
   </div>
  </div>
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('submenucontent[image]') ?>">
    <input class="icon-image" type="hidden" name="submenucontent[image]" value="<?php echo $content->image ?>" />
    <input class="icon-image-thumb" type="hidden" name="submenucontent[image_thumb]" value="<?php echo $content->image ?>" />
    <label>Image</label>
    <input type="file" class="file-input-ajax" name="upload_image" data-url="<?php echo base_admin("work/upload_file") ?>" data-target="icon-image" data-target-thumb="icon-image-thumb"/>
    <?php if ($content->image) { ?>
        <div class="upload-image-holder">
         <img src="<?php echo base_url() . $content->image ?>" />
         <span class="remove-image"><i class="glyphicon glyphicon-remove"></i></span>
        </div>
    <?php } ?>
    <?php echo form_error('submenucontent[image]') ?>
   </div>
  </div>
  <div class="col-lg-12">
   <button class="btn btn-primary" type="submit">Submit</button>
   <!--<button class="btn btn-default" type="reset">Reset</button>-->
  </div>
 </form>
</div>
<input type="hidden" id="base-url" value="<?php echo base_admin() ?>" />