<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery-ui.min.js"></script>
<input type="hidden" id="submenu-id" value="<?php echo $submenu->id ?>">
<div class="col-lg-12">
 <div class="panel panel-default">
  <div class="panel-heading">
   Content Listings
  </div>
  <!-- /.panel-heading -->
  <div class="panel-body">
   <div class="table-responsive">
    <div role="grid">
     <div id="action-wrapper" class="row">
      <div class="col-lg-10">
       <!--<label><input type="search" class="form-control input-sm" placeholder="Search"></label>-->
      </div>
      <div class="col-lg-2">
       <a href="<?php echo base_admin("submenucontent/form/$submenu->id") ?>" class="btn btn-primary btn-sm"><i class="glyphicon glyphicon-plus"></i> Create new content</a>
      </div>
     </div>

     <table class="table table-striped table-bordered table-hover no-footer">
      <thead>
       <tr>
        <th>S.No.</th>
        <th>Title</th>
        <th>Status</th>
        <th>Created on</th>
        <th>Last updated on</th>
        <th></th>
       </tr>
      </thead>
      <tbody id="draggablePanelList">
          <?php
          $i = 0;
          foreach ($contents as $content) {
              ?>
           <tr data-id="<?php echo $content->id ?>">
            <td><?php echo ++$i; ?></td>
            <td><?php echo $content->title ?></td>
            <td><?php
                if ($content->status == 'Publish')
                    echo '<i class="glyphicon glyphicon-eye-open green-text" title="Published"></i>';
                else
                    echo '<i class="glyphicon glyphicon-eye-close red-text" title="Unpublished"></i>';
                ?>
            </td>
            <td><?php echo date("M-d-Y h:i", $content->created_on); ?></td>
            <td><?php echo date("M-d-Y h:i", $content->updated_on) ?></td>
            <td>
             <a class="btn btn-warning btn-xs" href="<?php echo base_admin("submenucontent/edit/$content->id") ?>"><i class="glyphicon glyphicon-edit"></i> Edit</a>
             <a class="confirm-delete-btn btn btn-danger btn-xs" href="<?php echo base_admin("submenucontent/delete/$content->id") ?>"><i class="glyphicon glyphicon-trash"></i> Delete</a>
            </td>
           </tr>
       <?php } ?>
      </tbody>
     </table>

     <div class="row">
        <div class="col-lg-3">
            <?php echo form_dropdown('records', $recordsPerPage, $this->session->userdata('records_per_page'), 'id="records-per-page" class="form-control"'); ?>
            <label>records per page</label>
        </div>
      <div class="col-lg-9 text-right">
       <?php echo $this->pagination->create_links(); ?>
      </div>
     </div>
    </div>
   </div>
  </div>
  <!-- /.panel-body -->
 </div>
 <!-- /.panel -->
</div>
<script>
    (function(){
        $("#draggablePanelList").sortable({
        update: function(event, ui) {
            var data = {};
            var contentIDs = [];
            $("#draggablePanelList").find("tr").each(function(e, el) {
                contentIDs[e] = $(el).attr("data-id");
            });
            data = {contentIDs: contentIDs, submenuID: $("#submenu-id").val()};
            $.ajax({
                data: data,
                type: 'POST',
                url: '<?php echo base_admin("submenucontent/change_priority/") ?>'
            });
        }
    });
    }());
</script>