<div class="col-lg-12">
 <div class="panel panel-default">
  <!-- /.panel-heading -->
  <div class="panel-body">
   <div class="table-responsive">
    <div>
     <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example">
      <thead>
       <tr>
        <th>S.No.</th>
        <th>Title</th>
        <th>Priority</th>
        <th>Last updated on</th>
        <th></th>
       </tr>
      </thead>
      <tbody>
          <?php
          $i = 0;
          foreach ($submenus as $submenu) {
              ?>
           <tr>
            <td><?php echo ++$i; ?></td>
            <td><?php echo $submenu->title ?></td>
            <td><?php echo $submenu->priority ?></td>
            <td><?php echo date("M-d-Y h:i", $submenu->updated_on) ?></td>
            <td>
             <a class="btn btn-warning btn-sm" href="<?php echo base_admin("submenu/edit/$submenu->id") ?>"><i class="glyphicon glyphicon-edit"></i> Edit</a>
             <?php if($submenu->grid_type != "No content") { ?>
             <a class="btn btn-info btn-sm" href="<?php echo base_admin("submenucontent/view/$submenu->id") ?>"><i class="glyphicon glyphicon-book"></i> View Content</a>
             <?php } ?>
            </td>
           </tr>
       <?php } ?>
      </tbody>
     </table>

     <div class="row">
      <!--      <div class="col-lg-3">
             <label>
              <select class="form-control input-sm" aria-controls="dataTables-example" name="dataTables-example_length">
               <option value="10">10</option><option value="25">25</option>
               <option value="50">50</option><option value="100">100</option>
              </select> records per page
             </label>
            </div>-->
      <div class="col-lg-9 text-right">
       <?php echo $this->pagination->create_links(); ?>
      </div>
     </div>
    </div>
   </div>
  </div>
  <!-- /.panel-body -->
 </div>
 <!-- /.panel -->
</div>