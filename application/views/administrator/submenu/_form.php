<div class="row">
 <div class="col-lg-6">
  <form class="well" action="<?php echo $form_action ?>" method="POST">
   <div class="form-group <?php echo form_has_error('submenu[title]') ?>">
    <label>Title</label>
    <input class="form-control" name="submenu[title]" autofocus="" value="<?php echo $submenu->title ?>" />
    <?php echo form_error('submenu[title]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('submenu[priority]') ?>">
    <label>Priority</label>
    <input class="form-control" name="submenu[priority]" value="<?php echo $submenu->priority ?>" />
    <?php echo form_error('submenu[priority]') ?>
   </div>
   <button class="btn btn-primary" type="submit">Submit</button>
   <a class="btn btn-default" href="<?php echo base_admin("submenu/view/$submenu->menu_internal_name"); ?>">Cancel</a>
  </form>
 </div>
</div>
<input type="hidden" id="base-url" value="<?php echo base_admin() ?>" />