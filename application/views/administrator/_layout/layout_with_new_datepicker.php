<?php 
$this->load->view('admin/common/header_datepicker.php'); ?>
<div id="wrapper">
<!-- Sidebar -->
<?php $this->load->view('admin/common/sidebar'); ?>
<!-- /#sidebar-wrapper -->
<!-- Page Content -->
<a href="#menu-toggle" class="btn btn-default" id="menu-toggle">Toggle Menu</a>
<?php echo $content ?>
<!-- /#page-content-wrapper -->
</div>
<?php $this->load->view('admin/common/footer2'); ?>