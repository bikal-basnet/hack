<?php $this->load->view('administrator/_common/header'); ?>
<div id="wrapper">
 <!-- Sidebar -->
 <?php $this->load->view('administrator/_common/sidebar'); ?>
 <!-- /#sidebar-wrapper -->
 <!-- Page Content -->
 <div id="main-container">
  <nav id="top-naviagtion" class="text-right">
   <ul class="list-inline">
    <li><a href="<?php echo base_url('administrator/accountsetting') ?>"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
    <li><a href="<?php echo base_url('administrator/login/logout') ?>"><i class="glyphicon glyphicon-log-out"></i> Logout</a></li>
   </ul>
  </nav>
  <?php if (!empty($breadcrumb)) { ?>
      <ul class="breadcrumb">
          <?php
          $current_page = end($breadcrumb);
          foreach ($breadcrumb as $breadcrumb_menu => $url) {
              if($current_page == $url) {
                  echo '<li>' . $breadcrumb_menu . '</li>';
              }else {
                  echo '<li><a href="' . base_url("administrator/{$url}") . '">' . $breadcrumb_menu . '</a> </li>';
              }
          }
          ?>
      </ul>
  <?php } ?>
  <h2 class="page-header"><?php echo $module_title ?></h2>
  <?php
  if ($this->session->flashdata('action_result')) {
      ?>
      <div class="alert alert-<?php echo $this->session->flashdata('action_result') ?>">
          <?php echo $this->session->flashdata('action_message') ?>
      </div>
      <?php
  }
  ?>
  <?php echo $content ?>
 <!-- /#page-content-wrapper -->
</div>
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-body">
    Confirm delete action?
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <a href="#" class="btn btn-danger danger">Delete</a>
   </div>
  </div>
 </div>
</div>
<div class="modal fade" id="confirm-remove-image-holder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
 <div class="modal-dialog">
  <div class="modal-content">
   <div class="modal-body">
    Confirm remove action?
   </div>
   <div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
    <a id="confirm-remove-image" href="#" class="btn btn-danger" data-dismiss="modal">Remove</a>
   </div>
  </div>
 </div>
</div>
<input type="hidden" value="<?php echo base_url(); ?>">
<script src="<?php echo base_url() ?>administrator_resources/js/bootstrap.min.js"></script>
<script src="<?php echo base_url() ?>administrator_resources/js/common.js"></script>
<script>
    (function(){
          $("#records-per-page").on("change", function() {
            var recordsPerPage = $(this).val();
            $.ajax({
                data: {recordsPerPage: recordsPerPage},
                type: "POST",
                url: "<?php echo base_admin("dashboard/change_records_per_page") ?>"
            }).done(function() {
                window.location.reload();
            });
        })
    }())
</script>
<?php $this->load->view('administrator/_common/footer'); ?>