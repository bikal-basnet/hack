<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery-2.1.1.min.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/vendor/jquery.ui.widget.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.iframe-transport.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/jquery.fileupload.js"></script> 
<script src="<?php echo base_url() ?>administrator_resources/js/custom-upload.js"></script>
<div class="row well">
 <form action="<?php echo $form_action ?>" method="POST">
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('work[title]') ?>">
    <label>Title</label>
    <textarea class="form-control" name="work[title]" autofocus=""><?php echo $work->title ?></textarea>
    <!--<input class="form-control" name="work[title]" autofocus="" value="<?php echo $work->title ?>" />-->
    <?php echo form_error('work[title]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[icon_image]') ?>">
    <input id="icon-image" type="hidden" name="work[icon_image]" value="<?php echo $work->icon_image ?>" />
    <input id="icon-image-thumb" type="hidden" name="work[icon_image_thumb]" value="<?php echo $work->icon_image_thumb ?>" />
    <label>Icon Image</label>
    <small>(350 X 350)</small>
    <input type="file" class="file-input-ajax" name="upload_image" data-url="<?php echo base_admin("work/upload_file") ?>" data-target="#icon-image" data-target-thumb="#icon-image-thumb"/>
    <?php if ($work->icon_image_thumb) { ?>
        <div class="upload-image-holder">
         <img src="<?php echo base_url() . $work->icon_image_thumb ?>" />
         <span class="remove-image"><i class="glyphicon glyphicon-remove"></i></span>
        </div>
    <?php } ?>
    <?php echo form_error('work[icon_image]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[flip_image]') ?>">
    <input id="flip-image" type="hidden" name="work[flip_image]" value="<?php echo $work->flip_image ?>" />
    <input id="flip-image-thumb" type="hidden" name="work[flip_image_thumb]" value="<?php echo $work->flip_image_thumb ?>" />
    <label>Flip Image</label>
    <small>(350 X 350)</small>
    <input type="file" class="file-input-ajax" name="upload_image" data-url="<?php echo base_admin("work/upload_file") ?>" data-target="#flip-image" data-target-thumb="#flip-image-thumb"/>
    <?php if ($work->flip_image_thumb) { ?>
        <div class="upload-image-holder">
         <img src="<?php echo base_url() . $work->flip_image_thumb ?>" />
         <span class="remove-image"><i class="glyphicon glyphicon-remove"></i></span>
        </div>
    <?php } ?>
    <?php echo form_error('work[flip_image]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[cover_image]') ?>">
    <input id="cover-image" type="hidden" name="work[cover_image]" value="<?php echo $work->cover_image ?>" />
    <input id="cover-image-thumb" type="hidden" name="work[cover_image_thumb]" value="<?php echo $work->cover_image_thumb ?>" />
    <label>Project Cover Image</label>
    <small>(1024 X 768)</small>
    <input type="file" class="file-input-ajax" name="upload_image" data-url="<?php echo base_admin("work/upload_cover_image") ?>" data-target="#cover-image" data-target-thumb="#cover-image-thumb"/>
    <?php if ($work->cover_image_thumb) { ?>
        <div class="upload-image-holder">
         <img src="<?php echo base_url() . $work->cover_image_thumb ?>" />
         <span class="remove-image"><i class="glyphicon glyphicon-remove"></i></span>
        </div>
    <?php } ?>
    <?php echo form_error('work[cover_image]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[type]') ?>">
    <label>Type</label>
    <input class="form-control" name="work[type]" value="<?php echo $work->type ?>" />
    <?php echo form_error('work[type]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[location]') ?>">
    <label>Location</label>
    <input class="form-control" name="work[location]" value="<?php echo $work->location ?>">
    <?php echo form_error('work[location]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[client]') ?>">
    <label>Client</label>
    <input class="form-control" name="work[client]" value="<?php echo $work->client ?>">
    <?php echo form_error('work[client]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[work_status]') ?>">
    <label>Work Status</label>
    <input class="form-control" name="work[work_status]" value="<?php echo $work->work_status ?>">
    <?php echo form_error('work[work_status]') ?>
   </div>
  </div>
  <div class="col-lg-6">
   <div class="form-group <?php echo form_has_error('work[description]') ?>">
    <label>Description</label>
    <input class="form-control" name="work[description]" value="<?php echo $work->description ?>" />
    <?php echo form_error('work[description]') ?>
   </div>
   <div class="form-group <?php echo form_has_error('work[long_description]') ?>">
    <label>Long description</label>
    <textarea class="form-control" name="work[long_description]" style="height: 500px"><?php echo $work->long_description ?></textarea>
    <?php echo form_error('work[long_description]') ?>
   </div>
   <div class="form-group">
    <label>Status</label>
    <?php
    $options = array(
        'Publish' => 'Publish',
        'Unpublish' => 'Unpublish',
    );
    echo form_dropdown('work[status]', $options, $work->status, 'class="form-control"')
    ?>
   </div>
  </div>
  <div class="col-lg-12">
   <button class="btn btn-primary" type="submit">Submit</button>
   <a class="btn btn-default" href="<?php echo base_admin("work"); ?>">Cancel</a>
  </div>
 </form>
</div>
<input type="hidden" id="base-url" value="<?php echo base_admin() ?>" />