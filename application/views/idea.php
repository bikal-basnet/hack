<div class="content-area">
    <div class="container">
        <section style="padding-bottom: 50px; padding-top: 50px;">
            <div class="row">
                <div class="col-md-12">
                    <form action="<?php echo base_url("idea/add") ?>" method="post">
                    <div class="alert alert-info">
                        <h2>I have an Idea</h2>
                        <textarea style="width:100%" name="description" id="bio" rows="5"></textarea>
                    </div>
                    <div>
                        <button type="submit" class="btn btn-success" href="#">Send</button>
                        <br><br>
                    </div>
                    </form>
                </div>
            </div>
            <!-- ROW END -->
        </section>
    </div>
</div>