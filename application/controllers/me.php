<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Me extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        if(!$this->session->userdata('logged_in')) {
            redirect();
        }
    }

    function index() {
        $this->load->view('common/header', []);
        $user = $this->session->userdata('user');
        $data['user'] = $this->db->query("SELECT * FROM user WHERE id = $user->id")->row();
        $this->load->view('me', $data);
        $this->load->view('common/footer');
    }
}