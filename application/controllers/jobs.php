<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Jobs extends CI_Controller {

    function __construct() {
        parent::__construct();

    }

    function index() {
		$this->load->view('common/header', []);
        $data['jobs'] = $jobs = $this->db->query("SELECT * FROM job WHERE 1")->result();
        $data['totalJob'] = $totalJobs = count($jobs);
		$this->load->view('job', $data);
		$this->load->view('common/footer');
    }

    function detail() {
        $this->load->view('common/header', []);
        $data['user'] = $this->session->userdata('user');
        $data['job'] = $jobs = $this->db->query("SELECT * FROM job WHERE 1")->row();
        $this->load->view('job-detail', $data);
        $this->load->view('common/footer');
    }

    function apply() {
        $cover_letter = $this->input->post("cover_letter");
        $user = $this->session->userdata('user');
        $job_id = $this->input->post('job_id');
        $jobApplied = $this->db->query("SELECT * FROM job_application WHERE job_id = $job_id && user_id = $user->id")->row();
        if(!empty($jobApplied)) {
            redirect($_SERVER['HTTP_REFERER']);
        }
        echo $this->db->insert('job_application', ['job_id' => $job_id, 'user_id' => $user->id, 'cover_letter' => $cover_letter, 'applied_on' => date('Y-m-d h:i:s')]);
        exit;

    }
}