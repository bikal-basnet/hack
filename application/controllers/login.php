<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
    }

    function check() {
        header('Content-Type: application/json');
        $username = $this->input->post('username');
        $password = md5($this->input->post('password'));
        $user = $this->db->query("SELECT id, full_name,email, profile_image, username, bio, facebook,googleplus, twitter, linkedin, cv FROM user WHERE username = '$username' && password = '$password'")->row();
        if(!empty($user)) {
            $this->session->set_userdata('logged_in', true);
            $this->session->set_userdata('user', $user);
            echo json_encode(['logged_in' => 1, 'user' => $user]);
        }else {
            echo json_encode(['logged_in' => 0]);
        }
    }

    function updatePrimaryDetail() {
        $fullName = $this->input->post('fullName');
        $email = $this->input->post('email');
        if($fullName == '' OR $email == '') {
            redirect('me');
        }
        $user = $this->session->userdata('user');
        $this->db->where('id', $user->id)->update('user', ['full_name' => $fullName, 'email' => $email]);
        redirect("me");
    }

    function updateDetail() {
        $bio = $this->input->post('bio');
        $facebook = $this->input->post('facebook');
        $twitter = $this->input->post('twitter');
        $googleplus = $this->input->post('googleplus');
        $linkedin = $this->input->post('linkedin');

        $user = $this->session->userdata('user');
        $this->db->where('id', $user->id)->update('user',
            [
                'bio' => "$bio",
                'facebook' => "$facebook",
                'twitter'=> "$twitter",
                'googleplus'=> "$googleplus",
                'linkedin'=> "$linkedin"
            ]);
        redirect("me");
    }

    function updatePassword() {

    }
}