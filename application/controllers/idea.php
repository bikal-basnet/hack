<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

Class Idea extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('session');
        if(!$this->session->userdata('logged_in')) {
            redirect();
        }
    }

    function index() {
        $this->load->view('common/header', []);
        $this->load->view('idea');
        $this->load->view('common/footer');
    }

    function add() {
        $description = $this->input->post('description');
        $this->db->insert('idea', ['description' => $description]);
        redirect('idea');
    }
}