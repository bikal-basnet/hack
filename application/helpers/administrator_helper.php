<?php
	function current_admin_id()
	{
		$CI =& get_instance();
		return (int)$CI->session->userdata('admin_id');
	}
	
	function is_already_logged_in()
	{
		if ( ! current_admin_id())
		{
			$data['login'] = 'You must login to access this page!';
			$data['class'] = 'error';
			
			$CI =& get_instance();
			$CI->session->set_flashdata($data);
			redirect('login');
		}	
	}
	
	function site_alias($str)
	{
		$str = strtolower(trim($str));			
		$str = preg_replace('/[^a-z0-9-\s]/', '', $str);        //removing all the special characters                
		$str = preg_replace('/[\s]+/', '-', $str);                      //converting white space into hypen			
		$str = preg_replace('/-{2,}/', '-', $str);                      //removes more than 2 consecutive hypens 

		return $str;
	}
	
	function flash_redirect($page_name, $id)
	{
		$get = get_url();
		$page_name .= $get;
		
		if((int)$id != 0) 
		{
			$CI = & get_instance();
			
			if( ! $get)
				$page_name .= '?';
			else
				$page_name .= '&';			
			
			$page_name .= 'database=' . $id;
		}
		redirect($page_name);
	}
