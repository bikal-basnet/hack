<?php

	function get_today()
	{
		return date('Y-m-d');
	}
	
	function get_now()
	{
		return date('Y-m-d H:i:s');
	}
	
	function date_difference($day2, $day1 = '')
	{
		if($day1 == '') {
			$day1 = get_now();
		}
		
		if($day2 == '0000-00-00'){
			return '0';
		}
		$difference = strtotime($day2) - strtotime($day1);
		$difference = abs($difference) / 60 /60 /24;
		$difference = floor($difference);
		return $difference;
	}
	
	function printr($data, $exit = false)
	{
		echo "<pre>";
		print_r($data);
		echo "</pre>";
                if($exit)
                    exit;
	}	
	
	function convert_to_dropdown($object, $value, $index = 'id', $default_text = 'Select', $display_text = TRUE)
	{
		if($display_text || count($object) > 1)
			$tmp['']  = $default_text;
		else
			$tmp = array();
		foreach($object as $row) {
			$tmp[$row->$index] =  $row->$value;
		}
		return $tmp;
	}
	
	function dropdown_data($result, $index, $value, $rels = '')
	{
		$tmp = array();
		//$tmp['']  = 'Please select';
		foreach($result as $row) {
			if($rels) {
				$rel = '" ' . $rels . '="' . $row->$rels;
			} else {
				$rel = "";
			}			
			$tmp[$row->$index . $rel] =  $row->$value;
		}
		return $tmp;
	}
	
	function get_url()
	{
		$C = &get_instance();
		$get = $C->input->get();
		$url = '';
		if(is_array($get))
		{
			$i = 1;
			foreach($get as $index=>$value)
			{
				if( ! $url) {
					$url = '?';
				}
				
				$url .= $index . "=" . $value;
				if($i < count($get))
					$url .= '&';
				$i++;
			}
		}
		return ($url);
	}
?>
